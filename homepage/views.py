from django.shortcuts import render
from .models import Kegiatan
from .forms import Formnya
from django.views.decorators.http import require_POST

def beranda(request):
    return render(request, 'Beranda.html')
def galeri(request):
    return render(request, 'Galeri.html')
def kontak(request):
    return render(request, 'Kontak.html')
def pengalaman(request):
    return render(request, 'Pengalaman.html')
def tentang(request):
    return render(request, 'Tentang.html')
def kegiatan(request):
    list_kegiatan = Kegiatan.objects.order_by('tanggalKegiatan')
    form = Formnya()
    context = {'list_kegiatan' : list_kegiatan, 'form' : form}
    return render(request, 'Kegiatan.html', context)
# Create your views here.

@require_POST
def tambah_kegiatan(request):
    form = Formnya(request.POST)
    if form.is_valid():
        kegiatan_baru = Kegiatan(namaKegiatan = request.POST['namaKegiatan'],
                                 tanggalKegiatan = request.POST['tanggalKegiatan'],
                                 tempatKegiatan = request.POST['tempatKegiatan'],
                                 jamKegiatan = request.POST['jamKegiatan'],
                                 kategoriKegiatan = request.POST['kategoriKegiatan'])
        
        kegiatan_baru.save()
    return redirect('homepage:kegiatan')

def pilih(request, kegiatan_id):
    kegiatan = Kegiatan.objects.get(pk = kegiatan_id)
    if kegiatan.dipilih == False:
        kegiatan.dipilih = True
    else:
        kegiatan.dipilih == False
    
    kegiatan.save()

    return redirect('homepage:kegiatan')

def hapusKegiatan(request):
    Kegiatan.objects.filter(dipilih__exact = True).delete()

    return redirect('homepage:kegiatan')
