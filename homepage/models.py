from django.db import models

# Create your models here.
class Kegiatan(models.Model):
    namaKegiatan = models.CharField(max_length = 50)
    tanggalKegiatan = models.DateField()
    tempatKegiatan = models.CharField(max_length = 50)
    jamKegiatan = models.TimeField()
    kategoriKegiatan = models.CharField(max_length = 50)

    def __str__(self):
        return self.namaKegiatan
